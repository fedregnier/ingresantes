package com.adistec.ingresantes.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = RecaptchaCustomValidator.class)
public @interface Recaptcha {
    String message() default "You have failed this Recaptcha test. Please try again.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}