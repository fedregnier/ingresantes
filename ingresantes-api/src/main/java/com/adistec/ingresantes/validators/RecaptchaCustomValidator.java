package com.adistec.ingresantes.validators;

import com.github.mkopylec.recaptcha.validation.RecaptchaValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class RecaptchaCustomValidator implements ConstraintValidator<Recaptcha,String> {

    @Autowired
    private RecaptchaValidator recaptchaValidator;

    public RecaptchaCustomValidator() {
    }

    public void initialize(Recaptcha constraint) {
    }

    public boolean isValid(String recaptcha, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate("You have failed this Recaptcha test. Please try again.").addPropertyNode("recaptcha").addConstraintViolation();
        return this.recaptchaValidator.validate(recaptcha).isSuccess();
    }
}
