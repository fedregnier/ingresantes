package com.adistec.ingresantes.controller;

import com.adistec.ingresantes.dto.NewcomerDTO;
import com.adistec.ingresantes.service.NewcomerService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;

@RestController
@RequestMapping(value = "/newcomer")
public class NewcomerController {
	private static final Logger LOG = LoggerFactory.getLogger(NewcomerController.class);

	@Autowired
	private NewcomerService newcomerService;

	private static SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");


	@PostMapping(value = "")
	public ResponseEntity<?> send(@Validated @RequestBody NewcomerDTO newcomer, Errors errors) {
		try {
			if(errors.hasErrors())
				return new ResponseEntity<Object>(errors.getAllErrors(),HttpStatus.INTERNAL_SERVER_ERROR);
			LOG.info("Sending newcomer ");
			newcomerService.sendNewcomerMail(newcomer);
			LOG.info("Newcomer sent");
			return ResponseEntity.ok("success");
		} catch (Exception e) {
			LOG.error("Error while sending newcomer", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
