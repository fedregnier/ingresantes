package com.adistec.ingresantes.controller;

import com.adistec.ingresantes.utils.LocaleUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value = "/country")
public class CountryController {

	@Value("${microservice.baseUrl}")
    private String microServiceBaseUrl;
	
	@Autowired
	RestTemplate restTemplate;
	
	@RequestMapping(value="")
	   public ResponseEntity<?> getAllCountries(){
	       HttpHeaders headers = new HttpHeaders();
	       headers.set("locale", LocaleUtil.getLocale());
	       HttpEntity<?> entityReq = new HttpEntity<>(null, headers);
	       return restTemplate.exchange(microServiceBaseUrl + "/countries", HttpMethod.GET,entityReq,String.class);
	   }

}
