package com.adistec.ingresantes.service;

import com.adistec.ingresantes.dto.NewcomerDTO;

import java.io.IOException;
import java.net.URISyntaxException;

public interface NewcomerService {

	void sendNewcomerMail(NewcomerDTO newcomer) throws URISyntaxException, IOException;
}
