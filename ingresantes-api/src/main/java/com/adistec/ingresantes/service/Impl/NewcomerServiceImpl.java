package com.adistec.ingresantes.service.Impl;
import com.adistec.ingresantes.dto.NewcomerDTO;
import com.adistec.ingresantes.service.NewcomerService;
import com.adistec.mailclient.impl.MailClient;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.ISpringTemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.io.File;

@Service
public class NewcomerServiceImpl implements NewcomerService {

	@Autowired
	private MailClient mailClient;

    @Autowired
    private TemplateEngine templateEngine;
	
	@Value("${spring.mail.account.devsend}")
    private String devsendAccount;

    @Value("${spring.mail.account.cco}")
    private String cco;

	@Value("${spring.mail.account.to}")
    private String to;

	@Value("${spring.mail.template.newComer}")
    private String newComerTemplate;
	
	public void sendNewcomerMail(NewcomerDTO newcomer) throws URISyntaxException, IOException {
		Map<String, ?> vars = new HashMap<>();
        Context context = new Context();
        context.setVariable("model",newcomer);
        byte[] bytes = IOUtils.toByteArray(new ClassPathResource("templates/adistec-logo-blue.png").getInputStream());
        String logo = java.util.Base64.getEncoder().encodeToString(bytes);
        context.setVariable("logo",logo);
        String content = templateEngine.process(newComerTemplate,context);
        try {
            mailClient.sendEmailWithContent(new String[] {to}, content, "Adistec - RR.HH. - Formulario de Ingresantes",null, devsendAccount,null,new String[] {cco});
        } catch (URISyntaxException e) {
            throw new IOException("Couldn't send new comer mail.");
        }
	}
}