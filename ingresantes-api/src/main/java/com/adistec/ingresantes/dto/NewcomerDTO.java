package com.adistec.ingresantes.dto;

import com.adistec.ingresantes.validators.Recaptcha;

import java.util.Date;
import java.util.List;

public class NewcomerDTO {

	private Date birthDate;
	private String name;
	private String lastName;
	private String socialSecurityNumber;
	private String gender;
	private String maritalStatus;
	private String address;
	private String phone;
	private String personalEmail;
	private String country;
	private String degree;
	private String university;
	private List<String> certifications;
	private String level;

	@Recaptcha
	private String recaptcha;

	private List<ChildDTO> childs;
	private EmergencyContactDTO emergencyContact;

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPersonalEmail() {
		return personalEmail;
	}

	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getUniversity() {
		return university;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public List<String> getCertifications() {
		return certifications;
	}

	public void setCertifications(List<String> certifications) {
		this.certifications = certifications;
	}

	public List<ChildDTO> getChilds() {
		return childs;
	}

	public void setChilds(List<ChildDTO> childs) {
		this.childs = childs;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public EmergencyContactDTO getEmergencyContact() {
		return emergencyContact;
	}

	public void setEmergencyContact(EmergencyContactDTO emergencyContact) {
		this.emergencyContact = emergencyContact;
	}

	public String getRecaptcha() {
		return recaptcha;
	}

	public void setRecaptcha(String recaptcha) {
		this.recaptcha = recaptcha;
	}
}