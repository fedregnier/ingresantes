package com.adistec.ingresantes.configuration;

import com.adistec.mailclient.impl.MailClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;
import org.thymeleaf.spring5.ISpringTemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ITemplateResolver;

@Configuration
public class MailConfiguration {

    @Value("${spring.services.endpoint.mail}")
    private String mailSenderUrl;

    @Bean
    public MailClient mailClient() {
        MailClient client = new MailClient(mailSenderUrl);
        return client;
    }
}