package com.adistec.ingresantes.utils;

import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Arrays;
import java.util.List;

public class LocaleUtil {

    private static List<String> localeOptions = Arrays.asList("en","es","pt");

    public static String getLocale() {
        return localeOptions.stream().anyMatch(l -> l.compareTo(LocaleContextHolder.getLocale().getLanguage()) == 0) ? LocaleContextHolder.getLocale().getLanguage() : "en";
    }

}