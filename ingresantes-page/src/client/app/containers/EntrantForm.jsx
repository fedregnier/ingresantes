import React from 'react';
import { reduxForm ,FieldArray , validate, submit,change,reset,asyncValidate } from 'redux-form';
import { connect } from 'react-redux';
import { axios } from '../actions/axiosWrapper';
import {Loader, AutoComplete, Modal, TextField, Button, GeneralValidations, SelectField, DatePicker,DatePickerWithInput } from 'adistec-react-components';
import RadioButtonGroup from '../components/AdistecRadioButtonGroup.jsx';
import ChildrenList from '../components/ChildrenList.jsx';
import CertificationsList from '../components/CertificationsList.jsx';
import ReCaptcha from '../components/ReCaptcha.jsx';
import {FormattedMessage, formatMessage, injectIntl } from 'react-intl';
import { increaseLoaderCount, decreaseLoaderCount} from '../actions/loaderActions';
import sortBy from 'lodash/sortBy';


class EntranetForm extends React.Component{
    constructor(props) {
        super(props);
        this.state=
            {
                open: false,
                requestResponse: {
                    open: false,
                    message: '',
                    title: ''
                },
                countries:[]
            }
    }


    static contextTypes = {
        intl: React.PropTypes.object.isRequired
    }

    componentWillMount(){
      this.props.increaseLoaderCount();
      axios.get('/country').then(response => {
        this.setState({countries: sortBy(response.data,['value'])});
        this.props.decreaseLoaderCount();
      }).catch(error =>
        this.props.decreaseLoaderCount()
      );
    }


    handleSubmit(data) {
        this.props.increaseLoaderCount();
        axios.post(`/newcomer`, data).then(response => {
            this.props.decreaseLoaderCount();
            this.setState({requestResponse : {open:true, message: `${this.context.intl.formatMessage({id:'entrantForm.requestSentSuccess'})}`, title: `${this.context.intl.formatMessage({id:'entrantForm.requestTitleSuccess'})}`}});
            this.props.reset('EntranetForm');
            window.grecaptcha.reset();
            this.props.change('EntranetForm','recaptcha',null);
        }).catch(error =>
        {
            this.props.decreaseLoaderCount();
            this.setState({requestResponse : {open:true, message: `${this.context.intl.formatMessage({id:'entrantForm.requestSentError'})}`, title: `${this.context.intl.formatMessage({id:'entrantForm.requestTitleError'})}`}})
            window.grecaptcha.reset();
            this.props.change('EntranetForm','recaptcha',null);
        });
    }

    handleClose = () => {
        this.setState({requestResponse:{open: false, message:'', title:''}});
    };


    modalItems = () => {
        let actions = [<Button label={`${this.context.intl.formatMessage({id:'general.close'})}`} width='20' marginBottomTop="6" className="right" float='right' show onClick={this.handleClose} marginRight="10"/>];
        return actions;
    };

    render(){
        const { intl } = this.props;
        return (

            <div>
                <div className="a-section_intro a-gradient_blue a-padding_30 a-padding--bottom_lg a-padding--top_0 a-display_flex">
                    <div className="container">
                        <Modal
                            open={this.state.requestResponse.open}
                            title={this.state.requestResponse.title}
                            content={<div style={{marginLeft: '10px', marginTop: '15px'}}>{this.state.requestResponse.message}</div>}
                            width="35%"
                            height="2%"
                            actions={this.modalItems()}
                            maxHeight="2%"
                        />
                        <h2 className="center-align a-margin--bottom_0"><FormattedMessage id='entrantForm.introSectionText1'/></h2>
                        <h4 className="center-align"><FormattedMessage id='entrantForm.introSectionText2'/></h4>
                    </div>
                </div>
                <div className="a-solid_lightblue a-section_main">
                    <div className="container a-move--moveup_md">
                        <form ref="form" onSubmit={this.props.handleSubmit(this.handleSubmit.bind(this))}>
                            <div className="row">
                                <div className="col s12">
                                    <div className="a-card left a--margin-top_5 a--margin-bottom_15 ">
                                        <TextField name="name"
                                                   size="6"
                                                   label={<FormattedMessage id='entrantForm.nameLabel'/>}
                                                   validate={GeneralValidations.required}
                                                   clearButton={true}
                                        />
                                        <TextField name="lastName"
                                                   size="6"
                                                   label={<FormattedMessage id='entrantForm.lastNameLabel'/>}
                                                   validate={GeneralValidations.required}
                                                   clearButton={true}
                                        />
                                        <div className="col s6">
                                            <label className="a--label_float"><FormattedMessage id='entrantForm.genderLabel'/></label>
                                            <RadioButtonGroup name="gender"
                                                              className="a--radioButtonGroup-columns_2"
                                                              items={[
                                                                  {"value":"male","label":`${this.context.intl.formatMessage({id:'general.male'})}`},
                                                                  {"value":"female","label":`${this.context.intl.formatMessage({id:'general.female'})}`}
                                                              ]}
                                            />
                                        </div>
                                        <TextField name="socialSecurityNumber"
                                                   size="6"
                                                   label={<FormattedMessage id='entrantForm.socialSecurityNumberLabel'/>}
                                                   validate={GeneralValidations.required}
                                                   clearButton={true}
                                        />
                                        <TextField name="personalEmail"
                                                   size="6"
                                                   label={<FormattedMessage id='entrantForm.personalEmailLabel'/>}
                                                   validate={[GeneralValidations.email,GeneralValidations.required]}
                                                   clearButton={true}
                                        />
                                        <DatePickerWithInput name="birthDate"
                                                           clearButton={true}
                                                           key="2"
                                                           label={<FormattedMessage id='entrantForm.birthDateLabel'/>}
                                                           size="6"
                                                           validate={GeneralValidations.required}
                                        />
                                        <TextField name="maritalStatus"
                                                   size="6"
                                                   label={<FormattedMessage id='entrantForm.maritalStatusLabel'/>}
                                                   validate={GeneralValidations.required}
                                                   clearButton={true}
                                        />
                                        <TextField name="address"
                                                   size="6"
                                                   label={<FormattedMessage id='entrantForm.addressLabel'/>}
                                                   validate={GeneralValidations.required}
                                                   clearButton={true}
                                        />
                                        <TextField name="phone"
                                                   size="6"
                                                   label={<FormattedMessage id='entrantForm.phoneLabel'/>}
                                                   validate={GeneralValidations.required}
                                                   clearButton={true}
                                        />
                                        <AutoComplete name="country"
                                                      size="6"
                                                      label={<FormattedMessage id='entrantForm.countryLabel'/>}
                                                      items={this.state.countries.map(c => ({label:c.value,value:c.value}))}
                                                      validate={GeneralValidations.required}
                                                      clearButton
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col s12 m12">
                                    <h4 className="center-align"><FormattedMessage id='entrantForm.childrenText'/></h4>
                                    <div className="a-card left a--margin-top_5 a--margin-bottom_15 a-padding--0">
                                        <FieldArray name="childs" component={ChildrenList}/>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col s12 m12">
                                    <h4 className="center-align"><FormattedMessage id='entrantForm.emergencyContactText1'/></h4>
                                    <div className="a-card left a--margin-top_5 a--margin-bottom_15 ">
                                        <TextField name="emergencyContact.name"
                                                   size="3"
                                                   label={<FormattedMessage id='entrantForm.nameLabel'/>}
                                                   validate={GeneralValidations.required}
                                                   clearButton={true}
                                        />
                                        <TextField name="emergencyContact.lastName"
                                                   size="3"
                                                   label={<FormattedMessage id='entrantForm.lastNameLabel'/>}
                                                   validate={GeneralValidations.required}
                                                   clearButton={true}
                                        />
                                        <TextField name="emergencyContact.phone"
                                                   size="6"
                                                   label={<FormattedMessage id='entrantForm.emergencyPhoneLabel'/>}
                                                   validate={GeneralValidations.required}
                                                   clearButton={true}
                                        />
                                        <TextField name="emergencyContact.address"
                                                   size="6"
                                                   label={<FormattedMessage id='entrantForm.addressLabel'/>}
                                                   validate={GeneralValidations.required}
                                                   clearButton={true}
                                        />
                                        <TextField name="emergencyContact.relationship"
                                                   size="6"
                                                   label={<FormattedMessage id='entrantForm.emergencyRelationshipLabel'/>}
                                                   validate={GeneralValidations.required}
                                                   clearButton={true}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col s12 m12">
                                    <h4 className="center-align"><FormattedMessage id='entrantForm.studiesText1'/></h4>
                                    <div className="a-card left a--margin-top_5 a--margin-bottom_15 ">
                                        <SelectField clearButton={true}
                                                     name="level"
                                                     size="6"
                                                     label={<FormattedMessage id='entrantForm.levelLabel'/>}
                                                     items={[
                                                         {"value":"Secundario Incompleto","label":<FormattedMessage id='entrantForm.levelStudies.incompleteHighSchool'/>},
                                                         {"value":"Secundario","label":<FormattedMessage id='entrantForm.levelStudies.highSchool'/>},
                                                         {"value":"Terciario Incompleto","label":<FormattedMessage id='entrantForm.levelStudies.incompleteTertiary'/>},
                                                         {"value":"Terciario","label":<FormattedMessage id='entrantForm.levelStudies.tertiary'/>},
                                                         {"value":"Universitario Incompleto","label":<FormattedMessage id='entrantForm.levelStudies.incompleteCollege'/>},
                                                         {"value":"Universitario","label":<FormattedMessage id='entrantForm.levelStudies.college'/>}
                                                     ]}
                                                     validate={GeneralValidations.required}
                                        />
                                        <TextField name="degree"
                                                   size="6"
                                                   label={<FormattedMessage id='entrantForm.degreeLabel'/>}
                                                   validate={GeneralValidations.required}
                                                   clearButton={true}
                                        />
                                        <TextField name="university"
                                                   size="12"
                                                   label={<FormattedMessage id='entrantForm.universityLabel'/>}
                                                   validate={GeneralValidations.required}
                                                   clearButton={true}
                                        />
                                        <div className="col s12 m12">
                                            <h4 className="center-align"><FormattedMessage id='entrantForm.certificationsLabel'/></h4>
                                            <FieldArray name="certifications" component={CertificationsList}/>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            <div className="col s12 left a--margin-top_15 recaptcha-container">
                                <ReCaptcha name="recaptcha" siteKey="6LeLM0cUAAAAAMwietLF3QG7k9z1MHlqvj0kXH71" validate={GeneralValidations.required}/>
                            </div>
                            <div className="bottom-buttons-container right a-margin--top_md">
                                <Button
                                    type="submit"
                                    label={<FormattedMessage id='general.submit'/>}
                                    primary={true}
                                    className="submit large"
                                    keyboardFocused={true}
                                    width={'auto'}
                                />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

EntranetForm = reduxForm({
    form:'EntranetForm',
    enableReinitialize: true,
    validate,
    asyncValidate
})(EntranetForm);

export default injectIntl(
connect(null,{
    increaseLoaderCount,
    decreaseLoaderCount,
    reset,
    change
})(EntranetForm))