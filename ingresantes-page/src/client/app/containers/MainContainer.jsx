import React from 'react';
import { connect } from 'react-redux';
import { Loader , Footer , Header } from 'adistec-react-components';

class MainContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
		return (
	    	<div>
		    	{this.props.counter.ready != 0 ? <Loader /> : null}
		         <div>
                    <div className="appWrapper a-theme_modern">
                        <Header logoSrc={"assets/img/adistec-white.png"}/>
                        <div id="main-wrapper">
                            <main style={this.props.counter.ready != 0 ? {opacity:0.2} : null}>
                                {this.props.children}
                            </main>
                        </div>
                        <Footer/>
                    </div>
                </div>
	        </div>
        )
    }
}


export default connect(
    state => {
        return {
            counter: state.loader.counter,
        }
    }, null
)(MainContainer);
