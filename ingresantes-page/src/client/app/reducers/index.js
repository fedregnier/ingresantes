import { combineReducers } from 'redux';
import loaderReducer from './loaderReducer';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
	form: formReducer,
    loader:loaderReducer
});

export default rootReducer;
