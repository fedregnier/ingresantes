import React from 'react';
import {RadioButtonGroup} from 'redux-form-material-ui';
import {Field} from 'redux-form';
import { RadioButton } from 'material-ui/RadioButton';
import {FormattedMessage} from 'react-intl';

const CustomRadioButtonGroup = (props) => {
    const {input,meta,className,onChange,children} = props;
    return(
      <div>
        <RadioButtonGroup {...props}/>
        {meta.error && meta.touched ? <span className="error"><FormattedMessage id='form.field.required'/></span> : null}
      </div>
    );
}

const AdistecRadioButtonGroup = ({
    name,
    onChange,
    className,
    validate,
    items
}) => (
    <Field name={name}
    	component={CustomRadioButtonGroup}
      onChange={onChange}
    	className={className}
    	validate={validate}
      children={items.map(item => <RadioButton value={item.value} label={item.label}/>)}/>
);

export default AdistecRadioButtonGroup;
