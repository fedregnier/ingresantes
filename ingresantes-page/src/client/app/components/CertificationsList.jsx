import React from 'react';
import {Link} from 'react-router';
import {FormattedMessage, formatMessage, injectIntl } from 'react-intl';
import {TextField, GeneralValidations, Button } from 'adistec-react-components';


const CertificationsList = ({ fields, meta: { error } }) => (
  <table className="a--editableList" cellPadding={0} cellSpacing={0}>
            <tbody>
            {fields.map((element,index) =>
                <tr key={index}>
                    <td className="a--padding-topbottom-0">
                        <TextField name={`${element}`}
                                   className="input-nolabel"
                                   id={`certificationName${index}`}
                                   size="12"
                                   validate={GeneralValidations.required}
                        />
                    </td>
                    <td className="a--padding-topbottom-0 center"
                        style={{
                            width:'10%',
                            minWidth:'40px'
                        }}>
                        <button type="button" onClick={() => fields.remove(index)}><span className="material-icons">delete</span></button>
                    </td>
                </tr>
            )}
            </tbody>
            <tfoot>
            <tr>
                <td colSpan={"4"} className="center">
                    {<Button
                        onClick={() => fields.push()}
                        label={<FormattedMessage id='general.add'/>}
                        primary={true}
                        className="submit wAuto"
                        keyboardFocused={true}
                        width={'auto'}
                    />}
                </td>
            </tr>
            </tfoot>
  </table>)

export default CertificationsList;