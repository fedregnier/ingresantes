import React from 'react';
import {Link} from 'react-router';
import {FormattedMessage, formatMessage, injectIntl } from 'react-intl';
import {TextField, GeneralValidations, Button } from 'adistec-react-components';


const ChildrenList = ({ fields, meta: { error } }) => (
  <table className="a--editableList" cellPadding={0} cellSpacing={0}>
            {fields.length > 0 ? <thead>
            <th><FormattedMessage id='entrantForm.childName'/></th>
            <th><FormattedMessage id='entrantForm.childLastName'/></th>
            <th colSpan={"2"}><FormattedMessage id='entrantForm.childAge'/></th>
            </thead> : null}
            <tbody>
            {fields.map((element,index) =>
                <tr key={index}>
                    <td>
                        <TextField name={`${element}.name`}
                                   className="input-nolabel"
                                   id={`childName${index}`}
                                   size="12"
                                   validate={GeneralValidations.required}
                        />
                    </td>
                    <td>
                        <TextField name={`${element}.lastName`}
                                   className="input-nolabel"
                                   id={`childLastName${index}`}
                                   size="12"
                                   validate={GeneralValidations.required}
                        />
                    </td>
                    <td>
                        <TextField name={`${element}.age`}
                                   className="input-nolabel"
                                   id={`childAge${index}`}
                                   size="12"
                                   validate={[GeneralValidations.numbers,GeneralValidations.required]}
                        />
                    </td>
                    <td className="center"
                        style={{
                            width:'10%',
                            minWidth:'40px'
                        }}>
                        <button type="button" onClick={() => fields.remove(index)}><span className="material-icons">delete</span></button>
                    </td>
                </tr>
            )}
            </tbody>
            <tfoot>
            <tr>
                <td colSpan={"4"} className="center">
                    {<Button
                        onClick={() => fields.push()}
                        label={<FormattedMessage id='general.add'/>}
                        primary={true}
                        className="submit wAuto"
                        keyboardFocused={true}
                        width={'auto'}
                    />}
                </td>
            </tr>
            </tfoot>
  </table>)

export default ChildrenList;