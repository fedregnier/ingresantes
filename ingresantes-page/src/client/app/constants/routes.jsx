import React, {PropTypes} from 'react';
import {IndexRedirect, Route, Router} from 'react-router';
import EntrantForm from '../containers/EntrantForm.jsx';
import MainContainer from '../containers/MainContainer.jsx';


export default (
    <Router>
        <Route path="" component={MainContainer}>
        	<IndexRedirect to="/" />
            <Route path="/" component={EntrantForm} />
        </Route>
    </Router>
);
