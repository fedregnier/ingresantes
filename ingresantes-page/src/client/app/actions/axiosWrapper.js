import axiosClass from 'axios';

const instance = axiosClass.create({ baseURL: _API });

export let axios = instance;
