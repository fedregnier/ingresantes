import { axios } from './axiosWrapper';
import { AXIOS_REQUEST} from '../actions/types';

export const increaseLoaderCount = () => {
    return dispatch => {
        dispatch({type: AXIOS_REQUEST, payload: 1})
    }
};

export const decreaseLoaderCount = () => {
    return dispatch => {
        dispatch({type: AXIOS_REQUEST, payload: -1})
    }
};
